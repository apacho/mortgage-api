package com.mortgage.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table(name = "user")
@Getter
@Setter
@Entity
@ToString
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;
	@NotNull
	private String OperationType;
	private double propertyCost;
	private double deposit;
	private int noOfApplicants;
	private String employeeStatus;
	private String employeeOccupation;
	private String contractType;
	private String JobStarted;
	private String title;
	private String firstName;
	private String middleName;
	private String surname;
	private String DOB;
	private String contatctNumber;
	private String email;
	private String generateLogId;
	private String generatePassword;
	
}
