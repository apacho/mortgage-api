package com.mortgage.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table(name = "transaction_summary")
@Getter
@Setter
@Entity
@ToString
public class TransactionSummary {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long transactionId;
	private long accountId;
	private String comment;
	private double depositedAmount; 
	private String transactionDate;
}
