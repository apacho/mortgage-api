package com.mortgage.demo.scheduler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mortgage.demo.entity.Account;
import com.mortgage.demo.entity.TransactionSummary;
import com.mortgage.demo.entity.User;
import com.mortgage.demo.repository.AccountRepository;
import com.mortgage.demo.repository.TransactionSummaryRepository;
import com.mortgage.demo.repository.UserRepository;
import com.mortgage.demo.utils.CommonUtils;

@Component
public class CronScheduler {

	private static final Logger logger = LoggerFactory.getLogger(CronScheduler.class);
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	@Autowired
	AccountRepository accountRepo;
	@Autowired
	UserRepository userRepo;
	@Autowired
	TransactionSummaryRepository transactionSummaryRepo;

	@Scheduled(fixedDelayString = "${scheduler.frequency}")
	public void startCronDataProcessor() {
		logger.info("Cron Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));

		List<User> userList = userRepo.findAll();
		List<Long> temp = new ArrayList<>();
		temp.add(1L);
		temp.add(2L);
		/*
		 * userList.forEach(user -> { if (!Objects.isNull(user)) {
		 * temp.add(user.getUserId()); }
		 * 
		 * });
		 */

		Map<Long, List<Account>> result = accountRepo.findByUserIdIn(temp).stream()
				.collect(Collectors.groupingBy(Account::getUserId));
		result.forEach((k, v) -> {
			logger.info("Item : " + k + " Count : " + v);
			if (temp.contains(k)) {
				v.forEach(account -> {
					updateTransactionSummary(k, account);
				});
			}

		});
	}

	private void updateTransactionSummary(Long userId, Account account) {
		TransactionSummary summary = new TransactionSummary();
		summary.setAccountId(account.getAccountId());
		summary.setTransactionDate(CommonUtils.parseDateToString(LocalDateTime.now()));

		if (account.getAccountType().equalsIgnoreCase("Mortgage")) {
			summary.setComment("Mortgage account is updating {{USER}}" + userId);
			if (account.getBalance() <= 0) {
				account.setBalance(0.0);
				summary.setDepositedAmount(0);
				summary.setComment("Saving account is updating {{USER}} with Zero" + userId);
				transactionSummaryRepo.save(summary);
				accountRepo.save(account);
			} else {
				summary.setDepositedAmount(account.getBalance() + 200);
				account.setBalance(account.getBalance() + 200);
				transactionSummaryRepo.save(summary);
				accountRepo.save(account);
			}
		} else {

			if (account.getBalance() <= 0) {
				account.setBalance(0.0);
				summary.setDepositedAmount(0.0);
				summary.setComment("Saving account is updating {{USER}}" + userId);
				transactionSummaryRepo.save(summary);
				accountRepo.save(account);
			} else {
				summary.setDepositedAmount(account.getBalance() - 200);
				summary.setComment("Saving account is updating {{USER}} with Zero" + userId);
				account.setBalance(account.getBalance() - 200);
				transactionSummaryRepo.save(summary);
			}
		}

	}

}
