package com.mortgage.demo.serviceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.mortgage.demo.appconstant.ApplicationConstants;
import com.mortgage.demo.dto.LoginDto;
import com.mortgage.demo.dto.RegistrationDto;
import com.mortgage.demo.dto.RegistrationResponseDto;
import com.mortgage.demo.dto.ResponseDto;
import com.mortgage.demo.entity.Account;
import com.mortgage.demo.entity.User;
import com.mortgage.demo.exception.InvalidUser;
import com.mortgage.demo.repository.AccounRepository;
import com.mortgage.demo.repository.UserRepository;
import com.mortgage.demo.sevice.UserService;
import com.mortgage.demo.utils.CommonUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepo;

	@Autowired
	AccounRepository accountRepo;

	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	ModelMapper mapper = new ModelMapper();

	@Override
	public ResponseDto login(LoginDto loginDto) {
		logger.info("validating the user login information in the userserviceimplementation");

		if (loginDto.getUserName() == null || loginDto.getPassword() == null) {
			ResponseDto response = new ResponseDto();
			response.setMessage("please enter the username and password,it cannot be empty");
			response.setCode(HttpStatus.BAD_REQUEST.value());
			return response;
		}

		User userEntity = userRepo.findByGenerateLogId(loginDto.getUserName());

		if (userEntity == null) {
			ResponseDto response = new ResponseDto();
			response.setMessage("please enter valid user details");
			response.setCode(HttpStatus.BAD_REQUEST.value());

			return response;

		}

		if (loginDto.getUserName().equals(userEntity.getGenerateLogId())
				&& loginDto.getPassword().equals(userEntity.getGeneratePassword())) {
			ResponseDto vresponse = new ResponseDto();
			vresponse.setMessage("user logined successfully");
			vresponse.setCode(HttpStatus.ACCEPTED.value());

			return vresponse;

		} else {
			throw new InvalidUser(ApplicationConstants.LOGIN_FAILURE_MESSAGE.getMessage());

		}

	}

	@Override
	public RegistrationResponseDto registerUser(RegistrationDto registerDto) {

		logger.info("Registering the user service in user service impl");

		User registerUser = new User();
		Account account = new Account();

		Optional<User> userInfo = userRepo.findByEmail(registerDto.getEmail());

		if (userInfo.isPresent()) {
			throw new InvalidUser(ApplicationConstants.REGISTER_FAILURE.getMessage());

		}

		registerUser.setOperationType(registerDto.getOperationType());
		registerUser.setNoOfApplicants(registerDto.getNoOfApplicants());

		double minimumPropertyCost = registerDto.getPropertyCost();

		if (minimumPropertyCost < 100000) {
			logger.info("validating the user property cost");
			throw new InvalidUser(ApplicationConstants.COSTFAILUREMESSAGE.getMessage());

		}
		registerUser.setPropertyCost(registerDto.getPropertyCost());

		double minimumDepositCost = registerDto.getDeposit();

		if (minimumDepositCost < 1000) {
			logger.info("validating the user depost amount");
			throw new InvalidUser(ApplicationConstants.DEPOSITFAILUREMESSAGE.getMessage());

		}

		registerUser.setDeposit(registerDto.getDeposit());

		registerUser.setEmployeeStatus(registerDto.getEmployeeStatus());
		registerUser.setEmployeeOccupation(registerDto.getEmployeeOccupation());
		registerUser.setContractType(registerDto.getContractType());
		registerUser.setJobStarted(registerDto.getJobStartedYear());
		registerUser.setTitle(registerDto.getTitle());

		String firstName = registerDto.getFirstName();

		if (firstName.isEmpty()) {
			throw new InvalidUser(ApplicationConstants.FIRSTNAMEFAILURE.getMessage());
		}

		registerUser.setFirstName(registerDto.getFirstName());
		registerUser.setMiddleName(registerDto.getMiddleName());
		String surNameName = registerDto.getSurname();

		if (surNameName.isEmpty()) {
			throw new InvalidUser(ApplicationConstants.SURNAMEFAILURE.getMessage());
		}

		registerUser.setSurname(registerDto.getSurname());

		String dateOfBirth = registerDto.getDateOfBirth();
		LocalDate date = CommonUtils.parseDOBStringToDOBDate(dateOfBirth);
		int ageCalculation = CommonUtils.findYearDifference(date);
		if (ageCalculation < 18) {

			throw new InvalidUser(ApplicationConstants.AGELIMITFAILUREMESSAGE.getMessage());

		}

		registerUser.setDOB(registerDto.getDateOfBirth());
		registerUser.setContatctNumber(registerDto.getContatctNumber());
		registerUser.setEmail(registerDto.getEmail());

		List<Account> listOfAccounts = new ArrayList<Account>();

		// Here we are generating the use details
		int randomLoginId = (int) (Math.random() * 8000) + 1000;
		String LoginId = "user" + randomLoginId;

		registerUser.setGenerateLogId(LoginId);

		int randomPassword = (int) (Math.random() * 6000) + 1000;
		String password = "pass" + randomPassword;

		registerUser.setGeneratePassword(password);

		// Saving register user information
		userRepo.save(registerUser);

		// Temporarly we are generating the bank accounts and its balances to the user

		int randomSavings = (int) (Math.random() * 9000) + 1000;
		String savingAccount = "ACC" + randomSavings;

		account.setAccountNumber(savingAccount);
		account.setAccountType("SAVINGS");
		double savingBalance = (double) (Math.random() * 5000) + 1000;
		account.setBalance(savingBalance);
		Long userId = registerUser.getUserId();
		account.setUserId(userId);

		listOfAccounts.add(account);

		// accountRepo.save(account);

		Account account1 = new Account();

		int randomMortGage = (int) (Math.random() * 7000) + 1000;
		String morgageAccount = "MORT" + randomMortGage;
		RegistrationResponseDto response = new RegistrationResponseDto();

		account1.setAccountNumber(morgageAccount);
		account1.setAccountType("MORTGAGE");
		double mortgagaeBalance = (double) (Math.random() * 9000) + 2000;
		account1.setBalance(mortgagaeBalance);
		Long user = registerUser.getUserId();
		account1.setUserId(user);
		listOfAccounts.add(account1);

		accountRepo.saveAll(listOfAccounts);

		response.setMessage("Congrajulations ,your mortage has been granted : This is data you will need :");
		response.setGeneratedLoginId(LoginId);
		response.setGeneratedPassword(password);
		response.setCurrentAccountNumber(savingAccount);
		response.setMortageAccountNumber(morgageAccount);

		return response;
	}

}
