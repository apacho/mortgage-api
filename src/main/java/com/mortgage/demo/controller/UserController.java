package com.mortgage.demo.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mortgage.demo.dto.LoginDto;
import com.mortgage.demo.entity.TransactionSummary;
import com.mortgage.demo.repository.TransactionSummaryRepository;
import com.mortgage.demo.sevice.UserService;

import io.swagger.annotations.ApiOperation;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	TransactionSummaryRepository repository;
	Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * Here we taking login information from the user
	 * 
	 * @param userName:String
	 * @param password:String
	 *
	 *                        Once validating the loging details we return the
	 *                        response to the user
	 * 
	 * @return message:String
	 * @return ResponseCode:int
	 *
	 */
	@PostMapping("/login")
	@ApiOperation("authenticating the user details")
	public com.mortgage.demo.dto.ResponseDto authenticate(@RequestBody LoginDto loginDto) {

		logger.info("user login informatio in usercontroller");
		return userService.login(loginDto);

	}

	/**
	 * @param userId
	 * @return
	 */
	@GetMapping("/accountSummary/{userId}")
	@ApiOperation("authenticating the user details")
	public ResponseEntity<List<TransactionSummary>> accountSummary(@PathVariable long userId) {
		List<TransactionSummary> temp = repository.findReportByAccountId(userId);
		return new ResponseEntity<>(temp, new HttpHeaders(), HttpStatus.OK);

	}

}
