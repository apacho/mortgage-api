package com.mortgage.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mortgage.demo.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByGenerateLogId(String generatelogid);

	Optional<User> findByEmail(String email);

}
