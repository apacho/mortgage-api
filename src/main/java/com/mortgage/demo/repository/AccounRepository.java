package com.mortgage.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mortgage.demo.entity.Account;

@Repository
public interface AccounRepository extends JpaRepository<Account, Long> {

}
