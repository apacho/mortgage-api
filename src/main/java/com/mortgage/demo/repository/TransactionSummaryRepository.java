package com.mortgage.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mortgage.demo.entity.TransactionSummary;

@Repository
public interface TransactionSummaryRepository extends JpaRepository<TransactionSummary, Long>{
	
	@Query("select c from TransactionSummary c where c.accountId = :accountId")
	public List<TransactionSummary> findReportByAccountId(@Param("accountId") long accountId);

}
