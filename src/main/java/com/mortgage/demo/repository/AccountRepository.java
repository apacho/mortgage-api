package com.mortgage.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mortgage.demo.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	@Query("SELECT b.accountNumber, b.userId from Account b")
	List<Object[]> findAccounts();
	
	@Query("SELECT e FROM Account e WHERE e.userId IN :ids")
	public List<Account> findByUserIdIn(@Param("ids") Iterable<Long> ids);
	
	
	

}
