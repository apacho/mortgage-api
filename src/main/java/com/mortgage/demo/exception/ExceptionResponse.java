package com.mortgage.demo.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExceptionResponse {

	private String status;

	private String errorMessage;


}
