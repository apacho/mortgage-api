package com.mortgage.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MortgageApiMasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MortgageApiMasterApplication.class, args);
	}

}
