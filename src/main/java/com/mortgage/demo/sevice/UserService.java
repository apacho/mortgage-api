package com.mortgage.demo.sevice;

import org.springframework.stereotype.Service;

import com.mortgage.demo.dto.LoginDto;
import com.mortgage.demo.dto.RegistrationDto;
import com.mortgage.demo.dto.RegistrationResponseDto;
import com.mortgage.demo.dto.ResponseDto;

@Service
public interface UserService {

	public ResponseDto login(LoginDto loginDto);

	RegistrationResponseDto registerUser(RegistrationDto registerDto);

}
