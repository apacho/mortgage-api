package com.mortgage.demo.utils;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class CommonUtils {

	public static LocalDateTime parseStringToDate(String reg_date) {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return LocalDateTime.parse(reg_date, dtf);
	}

	public static int findYearDifference(LocalDate bDate) {

		int year = bDate.getYear();
		int month = bDate.getMonthValue();
		int date = bDate.getDayOfMonth();

		LocalDate bday = LocalDate.of(year, month, date);
		LocalDate today = LocalDate.now();
		Period age = Period.between(bday, today);
		int years = age.getYears();
		return years;
	}

	public static String parseDateToString(LocalDateTime reg_date) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return dtf.format(reg_date);

	}

	public static LocalDate parseDOBStringToDOBDate(String reg_date) {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(reg_date, dtf);
	}

}
