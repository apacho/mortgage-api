package com.mortgage.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RegistrationResponseDto {

	private String message;
	private String generatedLoginId;
	private String generatedPassword;
	private String currentAccountNumber;
	private String mortageAccountNumber;

}
