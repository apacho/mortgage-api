package com.mortgage.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RegistrationDto {

	private String operationType;
	private int noOfApplicants;
	private double propertyCost;
	private double deposit;
	private String employeeStatus;
	private String employeeOccupation;
	private String contractType;
	private String jobStartedYear;
	private String title;
	private String firstName;
	private String middleName;
	private String surname;
	private String dateOfBirth;
	private String contatctNumber;
	private String email;

}
