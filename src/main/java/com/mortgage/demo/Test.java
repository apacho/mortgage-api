package com.mortgage.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.hibernate.type.LocalDateType;

public class Test {
	@SuppressWarnings("empty-statement")
	public static void main(String[] args) {
		LocalDate bday = LocalDate.of(2019, 07, 20);
		LocalDate today = LocalDate.now();
		Period age = Period.between(bday, today);
		int years = age.getYears();
		System.out.println("number of years: " + years);

		/*
		 * String stringDOB = "1992-2-10";
		 * 
		 * DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); LocalDate
		 * d = LocalDate.parse(stringDOB, dtf);
		 * 
		 * // Calendar birthDay = new GregorianCalendar(d.getYear(), d.getMonth(), //
		 * d.getDate()); // Calendar birthDay = new GregorianCalendar(1995,
		 * Calendar.MAY, 19); Calendar today = new GregorianCalendar();
		 * today.setTime(new Date()); int yearsInBetween = today.get(Calendar.YEAR) -
		 * birthDay.get(Calendar.YEAR); int monthsDiff = today.get(Calendar.MONTH) -
		 * birthDay.get(Calendar.MONTH); long ageInMonths = yearsInBetween * 12 +
		 * monthsDiff; long age = yearsInBetween; System.out.println(" : " +
		 * ageInMonths); System.out.println(" : " + age);
		 */

		/*
		 * int randomLoginId = (int) (Math.random() * 8000) + 1000; String LoginId =
		 * "user" + randomLoginId;
		 * 
		 * int randomPassword = (int) (Math.random() * 6000) + 1000; String password =
		 * "pass" + randomPassword;
		 * 
		 * int randomSavings = (int) (Math.random() * 9000) + 1000; String savingAccount
		 * = "ACC" + randomSavings;
		 * 
		 * int randomMortGage = (int) (Math.random() * 7000) + 1000; String
		 * morgageAccount = "MORT" + randomMortGage;
		 * 
		 * System.out.println(LoginId + password + savingAccount + morgageAccount);
		 */

	}

}
