package com.mortgage.demo.serviceTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.configuration.MockitoConfiguration;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;

import com.mortgage.demo.dto.LoginDto;
import com.mortgage.demo.dto.RegistrationDto;
import com.mortgage.demo.dto.ResponseDto;
import com.mortgage.demo.entity.User;
import com.mortgage.demo.exception.InvalidUser;
import com.mortgage.demo.repository.UserRepository;
import com.mortgage.demo.serviceimpl.UserServiceImpl;

@RunWith(PowerMockRunner.class)
public class UserServiceTest {

	@InjectMocks
	UserServiceImpl userService;

	@Mock
	UserRepository userRepo;

	@Test
	public void setUp() {

	}

	@Test
	public void authencatePositiveTest() {

		LoginDto dto = new LoginDto();
		dto.setUserName("Ram");
		dto.setPassword("Password");

		User user = new User();
		user.setGeneratePassword("Password");
		user.setGenerateLogId("Ram");

		Mockito.when(userRepo.findByGenerateLogId(dto.getUserName())).thenReturn(user);

		ResponseDto response = userService.login(dto);

		assertEquals(HttpStatus.ACCEPTED.value(), response.getCode().intValue());

	}

	@Test(expected = InvalidUser.class)
	public void authencateNegativeTest() {

		LoginDto dto = new LoginDto();
		dto.setUserName("Ram");
		dto.setPassword("Password");

		User user = new User();
		user.setGeneratePassword("Ram");
		user.setGenerateLogId("Ram");

		Mockito.when(userRepo.findByGenerateLogId(Mockito.anyString())).thenReturn(user);

		ResponseDto response = userService.login(dto);

		assertEquals(HttpStatus.ACCEPTED, response.getCode());

	}

	@Test(expected = InvalidUser.class)
	public void userRegistrationNegativeTest1() {

		RegistrationDto registrationDto = new RegistrationDto();
		registrationDto.setOperationType("Buying 1st home");
		registrationDto.setNoOfApplicants(3);
		registrationDto.setPropertyCost(200000);
		registrationDto.setDeposit(200000);
		registrationDto.setEmployeeStatus("Employed");
		registrationDto.setEmployeeOccupation("Farmer");
		registrationDto.setContractType("Perminant");
		registrationDto.setJobStartedYear("2016-06-09");
		registrationDto.setTitle("Mr.");
		registrationDto.setFirstName("Ranga");
		registrationDto.setMiddleName("Rama");
		registrationDto.setSurname("Bun");
		registrationDto.setDateOfBirth("1994-02-02");
		registrationDto.setContatctNumber("93893889393");
		registrationDto.setEmail("Boss@gmail.com");

		User user = new User();
		user.setEmail("Ranga@gmail.com");

		Mockito.when(userRepo.findByGenerateLogId(Mockito.anyString())).thenReturn(user);

	}

}
